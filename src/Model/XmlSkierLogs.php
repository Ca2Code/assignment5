<?php
/**
  * This file is a part of the code used in IMT2571 Assignment 5.
  *
  * @author Rune Hjelsvold
  * @version 2018
  */

require_once('Club.php');
require_once('Skier.php');
require_once('YearlyDistance.php');
require_once('Affiliation.php');

/**
  * The class for accessing skier logs stored in the XML file
  */  
class XmlSkierLogs
{
    /**
      * @var DOMDocument The XML document holding the club and skier information.
      */  
    protected $doc;
    
    /**
      * @param string $url Name of the skier logs XML file.
      */  
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
            $this->xpath = new DOMXpath($this->doc);
    }
    
    /**
      * The function returns an array of Club objects - one for each
      * club in the XML file passed to the constructor.
      * @return Club[] The array of club objects
      */
    public function getClubs()
    {

        $clubs = array();
  $elements = $this->xpath->query('/SkierLogs/Clubs/Club');   
    
  foreach ($elements as $club) { 
      $name = $club->getElementsByTagName("Name")->item(0)->nodeValue;
      $city = $club->getElementsByTagName("City")->item(0)->nodeValue;
      $county = $club->getElementsByTagName("County")->item(0)->nodeValue;
      $c = new Club($club->getAttribute('id'), $name, $city, $county);
          array_push($clubs, $c); 
    }
        return $clubs;
    }
    /**
      * The function returns an array of Skier objects - one for each
      * Skier in the XML file passed to the constructor. The skier objects
      * contains affiliation histories and logged yearly distances.
      * @return Skier[] The array of skier objects
      */
    public function getSkiers()
    {
        $skiers = array();

  $count = 0;
      $elements = $this->xpath->query('/SkierLogs/Skiers/Skier');   
      $seasons = $this->xpath->query('/SkierLogs/Season');      
    
    foreach ($elements as $skier) {   
      $userName = $skier->getAttribute('userName');
      $firstName = $skier->getElementsByTagName("FirstName")->item(0)->nodeValue;
        $lastName = $skier->getElementsByTagName("LastName")->item(0)->nodeValue;
      $yearOfBirth = $skier->getElementsByTagName("YearOfBirth")->item(0)->nodeValue;
      
      $s = new Skier($userName, $firstName, $lastName, $yearOfBirth);
    
      foreach($seasons as $season) {
        foreach($season->getElementsByTagName("Skiers") as $skiersAffiliation) {
          foreach ($skiersAffiliation->getElementsByTagName("Skier") as $skierElement) { 
            if($userName == $skierElement->getAttribute('userName')) {
              if($skiersAffiliation->getAttribute('clubId')) {
                $affiliation = new Affiliation($skiersAffiliation->getAttribute('clubId'), $season->getAttribute('fallYear'));
                $s->addAffiliation($affiliation); 
              }
              foreach($skierElement->getElementsByTagName('Log') as $log) { 
                foreach ($log->getElementsByTagName('Entry') as $entry) { 
                  $distance = $entry->getElementsByTagName("Distance");
                  $count += $distance->item(0)->nodeValue;
                }
              }
              $yearlyDistance = new yearlyDistance($season->getAttribute('fallYear'), $count);
              $s->addYearlyDistance($yearlyDistance); 
              $count = 0;
            }
          }
        }
      }

    array_push($skiers, $s);
    }
        return $skiers;
    }
}
?>